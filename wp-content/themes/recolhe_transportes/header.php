<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Recolhe_Transportes
 */
global $configuracao;

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<!-- TOPO -->
		<header class="topo" >
			<div class="container">
				<div class="row">

					<!-- LOGO -->
					<div class="col-sm-3">
						<a href="<?php echo home_url('/'); ?>" class="logo">
							<img src="<?php echo $configuracao['opt_logo']['url'] ?> " alt="<?php echo get_bloginfo( 'name' );  ?> " class="img-responsive">
						</a>
					</div>

					<!-- MENU -->
					<div class="col-sm-9">
						<div class="navbar" role="navigation">	

							<!-- MENU MOBILE TRIGGER -->
							<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop" data-toggle="collapse" data-target="#collapse">
								<span class="sr-only"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>

							<!--  MENU MOBILE-->
							<div class="row navbar-header">			
								<nav class="collapse navbar-collapse" id="collapse">
								<?php 
									$menu = array(
										'theme_location'  => '',
										'menu'            => 'Menu Principal',
										'container'       => false,
										'container_class' => '',
										'container_id'    => '',
										'menu_class'      => 'nav navbar-nav',
										'menu_id'         => '',
										'echo'            => true,
										'fallback_cb'     => 'wp_page_menu',
										'before'          => '',
										'after'           => '',
										'link_before'     => '',
										'link_after'      => '',
										'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
										'depth'           => 2,
										'walker'          => ''
										);
									wp_nav_menu( $menu );
								?>
								</nav>						
							</div>			
						</div>
					</div>

				</div>
			</div>
		</header>
<script>
	$(".navbar-collapse ul").addClass('nav navbar-nav');
	$(".navbar-collapse ul li a").addClass('scrollTop');
	$("#collapse > div").removeClass('nav navbar-nav');
</script>