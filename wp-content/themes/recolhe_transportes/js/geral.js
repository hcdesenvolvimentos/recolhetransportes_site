
$(function(){

	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	$(document).ready(function() {

		$('a.scrollTop').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
			
		});

		$('.page-template-empresa #menu-item-53').click(function(e) {
			window.location.href = "http://hudsoncarolino.carollino.com.br/front-end-projetos/ui/recolhe_transportes/#contato";
		});
		$('.single-servico  #menu-item-53').click(function() {
			window.location.href = "http://hudsoncarolino.carollino.com.br/front-end-projetos/ui/recolhe_transportes/#contato";
		});

		$('.page-template-empresa #menu-item-54').click(function(e) {
			window.location.href = "http://hudsoncarolino.carollino.com.br/front-end-projetos/ui/recolhe_transportes/#quemSomos";
		});
		$('.single-servico  #menu-item-54').click(function(e) {
			window.location.href = "http://hudsoncarolino.carollino.com.br/front-end-projetos/ui/recolhe_transportes/#quemSomos";
		});

		//CARROSSEL DE DESTAQUE
		$("#carrosselDestaque").owlCarousel({
			items : 1,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,
			
		});
		var carrosselDestaque = $("#carrosselDestaque").data('owlCarousel');
		$('#carrosselDestaqueLeft').click(function(){ carrosselDestaque.prev(); });
		$('#carrosselDestaqueRight').click(function(){ carrosselDestaque.next(); });	
	});
	var testeMostrarNum = false;
	$(window).scroll( function(){

        $('.positionLeft').each( function(i){
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            if( bottom_of_window > bottom_of_object ){
            	$(this).css('opacity', '1');
                $(this).addClass( "fadeInLeft animated" );
            }
        });

        $('.positionRight').each( function(i){
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            if( bottom_of_window > bottom_of_object ){
            	$(this).css('opacity', '1');
                $(this).addClass( "fadeInRight animated" );
            }
        });

         $('.centro').each( function(i){
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            if( bottom_of_window > bottom_of_object ){
            	$(this).css('opacity', '1');
                $(this).addClass( "fadeIn animated" );
            }
        });

    });

    $("a#fancy").fancybox({
		'titleShow' : false,
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		closeBtn    : true,
		arrows      : true,
		nextClick   : true
	});

	$(document).ready(function(){
		    var speed = 1000;

		    // check for hash and if div exist... scroll to div
		    var hash = window.location.hash;
		    if($(hash).length) scrollToID(hash, speed);

		    // scroll to div on nav click
		    $('.header ul li a').click(function (e) {
		        e.preventDefault();
		        var id = $(this).data('id');
		        var href = $(this).attr('href');
		        if(href === '#'){
		            scrollToID(id, speed);
		        }else{
		            window.location.href = href;
		        }

		    });
	    	
	    	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	    	      var target = $(this.hash);
	    	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	    	      if (target.length) {
	    	        $('html,body').animate({
	    	          scrollTop: target.offset().top
	    	        }, 1000);
	    	        return false;
	    	      }
	    	    }
		})

		function scrollToID(id, speed) {
		    var offSet = 70;
		    var obj = $(id).offset();
		    var targetOffset = obj.top - offSet;
		    $('html,body').animate({ scrollTop: targetOffset }, speed);
		}
});
