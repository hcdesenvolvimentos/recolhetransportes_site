<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Recolhe_Transportes
 */
global $configuracao;
?>

	<footer class="rodape" style="background: url(<?php echo $configuracao['opt_fundorodape']['url']?>)">
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="mapa">
							<strong>Mapa do site</strong>
							<?php 
								$menu = array(
									'theme_location'  => '',
									'menu'            => 'Menu Principal',
									'container'       => false,
									'container_class' => '',
									'container_id'    => '',
									'menu_class'      => 'nav navbar-nav',
									'menu_id'         => '',
									'echo'            => true,
									'fallback_cb'     => 'wp_page_menu',
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									'depth'           => 2,
									'walker'          => ''
									);
								wp_nav_menu( $menu );
							?>
						</div>
					</div>

					<div class="col-sm-4">
						<div class="mapa">
							<strong>Contato/Endereço</strong>
							<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco'] ?>" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $configuracao['opt_endereco'] ?></a>
							<a href="malito:<?php echo $configuracao['opt_Email'] ?>" target="_blank"><i class="fa fa-envelope" aria-hidden="true"></i> <?php echo $configuracao['opt_Email'] ?></a>
							<a href="tel:<?php echo $configuracao['opt_telefone'] ?>" target="_blank"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone'] ?></a>
							
						</div>
					</div>
					
					<div class="col-sm-4">
						<div class="mapa galeria">
							<strong>Galeria</strong>
							<?php 
								$i = 0;
								$galeria = explode(',', $configuracao['opt-quem-somos-galeria']);
					   			foreach($galeria as $galeria):
					   				if ($i <6):
					   					
							?>
							<a id="fancy" rel="gallery1" href="<?php echo wp_get_attachment_url( $galeria ); ?>">
								<img src="<?php echo wp_get_attachment_url( $galeria ); ?>" alt="Foto Galeria">
							</a>
							<?php endif;$i++;endforeach; ?>
							
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="container copyright">
				<div class="row">
					<div class="col-sm-6">
						<p><i class="fa fa-copyright" aria-hidden="true"></i> <?php echo $configuracao['opt_Copyryght'] ?></p>
					</div>
					<div class="col-sm-6 text-right">
						<p>Desenvolvido por <a href="http://hudsoncarolino.com.br/"><img src="<?php bloginfo('template_directory'); ?>/img/logo2.svg" alt="Hudson Carolino Desenvolvedor Web"></a></p>
					</div>
				</div>
			</div>
		</footer>

<?php wp_footer(); ?>

</body>
</html>
