<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Recolhe_Transportes
 */
$fotoServico = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$fotoServico = $fotoServico[0];
get_header(); ?>

	<!-- PÁGINA DE SERVIÇO -->
	<div class="pg pg-servico">
		<figure class="bannerTop" style="background: url(<?php echo $fotoServico ?>)">
			<h1><?php echo get_the_title() ?></h1>
		</figure>

		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<aside>
						<ul>
							<?php 
								//LOOP DE POST SERVIÇOS
								$servios = new WP_Query( array( 'post_type' => 'servico', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => 3) );
								while ( $servios->have_posts() ) : $servios->the_post();
							?>	
							<li><a href="<?php echo get_permalink() ?> "><?php echo get_the_title() ?></a></li>
							<?php endwhile; wp_reset_query(); ?>
						</ul>

						<div class="contato">
							<strong>Contato</strong>
							<p><?php echo $configuracao['opt_telefone'] ?></p>
						</div>
					</aside>
				</div>
				<div class="col-sm-9">
					<section class="conteudo">
						<figure style="background: url(<?php echo $fotoServico ?>)"></figure>
						
						<article>
							<?php echo the_content() ?>
						</article>

					</section>
				</div>
			</div>
		</div>
	</div>

<?php
get_footer();
