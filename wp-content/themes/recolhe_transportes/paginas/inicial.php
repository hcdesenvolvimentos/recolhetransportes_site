<?php
/**
 * Template Name: Inicial
 * Description: Página inicial
 *
 * @package Recolhe_Transportes
 */

get_header(); ?>
<!-- PÁGINA INICIAL -->
<div class="pg pg-inicial">

	<!-- SESSÃO DESTAQUE -->
	<section class="sessaoDestaque">
		<h6 class="hidden">Destaque</h6>
		
		<div class="btnCarrossel">
			<button id="carrosselDestaqueLeft"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
			<button id="carrosselDestaqueRight"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
		</div>

		<div id="carrosselDestaque" class="owl-Carousel">
			<?php 
				//LOOP DE POST DESTAQUES
				$posDestaques = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
				while ( $posDestaques->have_posts() ) : $posDestaques->the_post();
				$fotoDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$fotoDestaque = $fotoDestaque[0];
				$destaque_link = rwmb_meta('TransportesRescolhes_destaque_link');
			?>
			<!-- ITEM CARROSSEL -->
			<a href="<?php echo $destaque_link ?> " class="item" style="background: url(<?php echo $fotoDestaque  ?> )">
				<div class="container">
					<div class="textoDestaque">
						<h2><?php echo get_the_title() ?></h2>
						<p><?php echo get_the_content() ?></p>
						<span>Angendar</span>
					</div>
				</div>		
			</a>
			<?php endwhile; wp_reset_query(); ?>
		</div>
	</section>

	<section class="sessaoServicos" id="servicos">
		<div class="titulo">
			<h6 class=""><?php echo $configuracao['inicial_titulo_servico'] ?></h6>
			<p><?php echo $configuracao['inicial_subtitulo_servico'] ?></p>
		</div>
		
		<div class="container">
			<div class="row">
				<?php 
					//LOOP DE POST SERVIÇOS
					$servios = new WP_Query( array( 'post_type' => 'servico', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => 3) );
					while ( $servios->have_posts() ) : $servios->the_post();
						$fotoServico = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoServico = $fotoServico[0];

				?>
				<div class="col-sm-4">
					<a href="<?php echo get_permalink() ?> " class="servico">
						<figure style="background: url(<?php echo $fotoServico ?>)"></figure>
						<h2><?php echo get_the_title() ?></h2>
						<p><?php customExcerpt(200); ?></p>
					</a>
				</div>
				<?php endwhile; wp_reset_query(); ?>
			</div>
		</div>

		<div class="text-center">
			<?php 
				//LOOP DE POST SERVIÇOS LINK
				$serviosLink = new WP_Query( array( 'post_type' => 'servico', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => 1) );
				while ( $serviosLink->have_posts() ) : $serviosLink->the_post();
			?>
			<a href="<?php echo get_permalink() ?> " class="verMais">Ver todos os serviços</a>
			<?php endwhile; wp_reset_query(); ?>
		</div>
	</section>

	<section class="valoresEmpresa" id="valoresEmpresa" style="background: url(<?php echo $configuracao['inicial_sobreEmpresa_background']['url'] ?>);">

		<div class="container">
			<div class="valores">
				<div class="row">
					
					<div class="col-sm-3">
						<div class="valor">
							<img src="<?php echo $configuracao['inicial_sobreEmpresa_icone1']['url'] ?>" alt="<?php echo $configuracao['inicial_sobreEmpresa_icone1_texto1'] ?>">
							<div class="dados">
								<span  data-stop="<?php echo $configuracao['inicial_sobreEmpresa_icone1_valor1'] ?>" class="number">0</span>
								<p><?php echo $configuracao['inicial_sobreEmpresa_icone1_texto1'] ?></p>
							</div>
							
						</div>
					</div>

					<div class="col-sm-3">
						<div class="valor">
							<img src="<?php echo $configuracao['inicial_sobreEmpresa_icone2']['url'] ?>" alt="<?php echo $configuracao['inicial_sobreEmpresa_icone2_texto2'] ?>">
							<div class="dados">
								<span  data-stop="<?php echo $configuracao['inicial_sobreEmpresa_icone2_valor2'] ?>" class="number">0</span>
								<p><?php echo $configuracao['inicial_sobreEmpresa_icone2_texto2'] ?></p>
							</div>
							
						</div>
					</div>

					<div class="col-sm-3">
						<div class="valor">
							<img src="<?php echo $configuracao['inicial_sobreEmpresa_icone3']['url'] ?>" alt="<?php echo $configuracao['inicial_sobreEmpresa_icone3_texto3'] ?>">
							<div class="dados">
								<span  data-stop="<?php echo $configuracao['inicial_sobreEmpresa_icone3_valor3'] ?>" class="number">0</span>
								<p><?php echo $configuracao['inicial_sobreEmpresa_icone3_texto3'] ?></p>
							</div>
							
						</div>
					</div>

					<div class="col-sm-3">
						<div class="valor">
							<img src="<?php echo $configuracao['inicial_sobreEmpresa_icone4']['url'] ?>" alt="<?php echo $configuracao['inicial_sobreEmpresa_icone4_texto4'] ?>">
							<div class="dados">
								<span  data-stop="<?php echo $configuracao['inicial_sobreEmpresa_icone4_valor4'] ?>" class="number">0</span>
								<p><?php echo $configuracao['inicial_sobreEmpresa_icone4_texto4'] ?></p>
							</div>
							
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</section>

	<section class="sessaoInfoServicos" id="comoFazemos" style="background: url(<?php echo $configuracao['inicial_sobreEmpresa_outro_background']['url'] ?>);">
		<div id="quemSomos"></div>
		<div class="titulo">
			<h6><?php echo $configuracao['inicial_titulo_outros_servico'] ?></h6>
			<p><?php echo $configuracao['inicial_subtitulo_outros_servico'] ?></p>
		</div>

		<div class="container">
			
			<div class="areaInformacoes">
				<div class="row">
					<?php 
						$i = 1;
						 $posicao = array(
		            		1 => "positionLeft",
		            		2 => "centro",
		            		3 => "positionRight",
		            		4 => "positionLeft",
		            		5 => "centro",
		            		6 => "positionRight",
		            	);
						//LOOP DE POST DESTAQUES
						$outrosServicos = new WP_Query( array( 'post_type' => 'outros-servicos', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => 6) );
						while ( $outrosServicos->have_posts() ) : $outrosServicos->the_post();
						$fotooutroServico = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotooutroServico = $fotooutroServico[0];
						$descrcao = rwmb_meta('TransportesRescolhes_outroServico_descricao');
						if ($i == 7) { $i = 0;}
					?>
					<div class="col-md-4">

						<div class="areainfo <?php echo $posicao[$i] ?>">
							<div class="row">
								<div class="col-sm-3">
									<figure>
										<img src=" <?php echo $fotooutroServico ?>" alt="<?php echo get_the_title() ?>">
									</figure>
								</div>
								<div class="col-sm-9">
									<h2><?php echo get_the_title() ?></h2>
									<p><?php echo $descrcao ?></p>
								</div>
							</div>
						</div>
					</div>
					<?php $i++;endwhile; wp_reset_query(); ?>
				</div>

				
			</div>

		</div>
	</section>

	<section class="sessaoContato" id="contato">
		<div class="titulo">
			<h6><?php echo $configuracao['inicial_titulo_contato'] ?></h6>
			<p><?php echo $configuracao['inicial_subtitulo_contato'] ?></p>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-sm-7">
					<div class="form">
						<p><?php echo $configuracao['inicial_titulo_contato_form'] ?></p>
						<?php echo do_shortcode('[contact-form-7 id="4" title="Formulário de contato 1"]'); ?>
					</div>
				</div>
				<div class="col-sm-5">
					<div class="infocontato">
						<p><?php echo $configuracao['inicial_subtitulo_contato_form'] ?></p>
						

						<div class="caixaEndereco">
							<div class="row">
								<div class="col-xs-3">
									<i class="fa fa-map-marker" aria-hidden="true"></i>
								</div>
								<div class="col-xs-9">
									<h6>Endereço</h6>
									<p><?php echo $configuracao['opt_endereco'] ?></p>
								</div>
							</div>
						</div>
						<div class="caixaEndereco">
							<div class="row">
								<div class="col-xs-3">
									<i class="fa fa-envelope" aria-hidden="true"></i>
								</div>
								<div class="col-xs-9">
									<h6>E-mail</h6>
									<p><?php echo $configuracao['opt_Email'] ?></p>
								</div>
							</div>
						</div>
						<div class="caixaEndereco">
							<div class="row">
								<div class="col-xs-3">
									<i class="fa fa-phone" aria-hidden="true"></i>
								</div>
								<div class="col-xs-9">
									<h6>Telefone</h6>
									<p><?php echo $configuracao['opt_telefone'] ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script>
	$.fn.isOnScreen = function(){
		var viewport = {};
		viewport.top = $(window).scrollTop();
		viewport.bottom = viewport.top + $(window).height();
		var bounds = {};
		bounds.top = this.offset().top;
		bounds.bottom = bounds.top + this.outerHeight();
		return ((bounds.top <= viewport.bottom) && (bounds.bottom >= viewport.top));
	};
	var testeMostrarNum = false;
	$(window).scroll( function(){

         if ($('#valoresEmpresa').isOnScreen() == true && testeMostrarNum == false) {
         	$("span.number").each(function() {
         		var t = $(this);
         		$({
         			Counter: 0
         		}).animate({
         			Counter: t.attr("data-stop")
         		}, {
         			duration: 5e3,
         			easing: "swing",
         			step: function(a) {
         				t.text(Math.ceil(a))
         			}
         		})
         	});
         	testeMostrarNum = true;
         }

    });
</script>
<?php get_footer(); ?>