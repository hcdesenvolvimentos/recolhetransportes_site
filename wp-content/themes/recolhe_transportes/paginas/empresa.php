<?php
/**
 * Template Name: A Empresa
 * Description: A Empresa
 *
 * @package Recolhe_Transportes
 */
$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$foto = $foto[0];

get_header(); ?>
	<!-- PÁGINA QUEM SOMOS -->
		<div class="pg pg-quemSomos">
			<figure class="bannerTop" style="background: url(<?php echo $foto ?>)">
				<h1><?php echo the_title() ?></h1>
			</figure>

			<section class="quemSomos conteudo">
				<article class="container">
					<?php echo the_content() ?>
				</article>
			</section>
		</div>

<?php get_footer(); ?>